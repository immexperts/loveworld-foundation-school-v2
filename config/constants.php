<?php
/**
 * ----------------------------------------- *
 * File         : constants.php
 * Created for  : lwfs
 *
 * @Author      : Felix Anyanwu
 * @Email       : anyanwufelix@gmail.com
 * @Phone       : +2348133463582
 * Date         : 03/02/2021
 * Time         : 13:41
 * ----------------------------------------- *
 */
return [
    'dev'   => 'New Media Technologies',
    'site'  => 'LoveWorld Foundation School'
];