<?php
$home_menu = isset($home_menu) ? 'active-menu' : '';
$resource_menu = isset($resource_menu) ? 'active-menu' : '';
$assignment_menu = isset($assignment_menu) ? 'active-menu' : '';
$badge_menu = isset($badge_menu) ? 'active-menu' : '';
$report_menu = isset($report_menu) ? 'active-menu' : '';
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{{$page_title}} : {{config('constants.site')}}</title>
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="icon" href="{{url('assets/images/logo.png')}}" type="image/png">
    <!-- Bootstrap Styles-->
    <link href="{{url('assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.css" integrity="sha512-2eMmukTZtvwlfQoG8ztapwAH5fXaQBzaMqdljLopRSA0i6YKM8kBAOrSSykxu9NN9HrtD45lIqfONLII2AFL/Q==" crossorigin="anonymous" />
    <!-- Custom Styles-->
    <link href="{{url('assets/css/custom-styles.css')}}" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route('home')}}">
                <img src="{{url('assets/images/logo.png')}}" width="50px">
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="{{route('logout')}}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
    </nav>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li class="{{$home_menu}}">
                    <a href="{{route('home')}}"><i class="fa fa-home"></i> Home</a>
                </li>

                @if(session('delegate')->role == 'principal')
                <li class="{{$report_menu}}">
                    <a href="{{route('reports')}}"><i class="fa fa-pencil"></i> Weekly Report</a>
                </li>
                @endif

                <li class="{{$resource_menu}}">
                    <a href="{{route('viewResources')}}"><i class="fa fa-book"></i> Resources</a>
                </li>
                <li class="{{$assignment_menu}}">
                    <a href="{{route('assignments')}}"><i class="fa fa-file"></i> Assignments</a>
                </li>

                <li class="{{$badge_menu}}">
                    <a href="{{route('badge')}}"><i class="fa fa-id-badge"></i> Badge</a>
                </li>

            </ul>

        </div>

    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('error'))
                    <div class="alert alert-danger">{{session('error')}}</div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">{{session('message')}}</div>
                        @endif
                </div>
            </div>
            @yield('content')
            <!-- /. ROW  -->
            <footer><p>All right reserved. <a href="{{route('home')}}">{{config('constants.dev')}}</a></p></footer>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- JS Scripts-->
<!-- jQuery Js -->
<script src="{{url('assets/js/jquery-1.10.2.js')}}"></script>
<!-- Bootstrap Js -->
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<!-- Metis Menu Js -->
<script src="{{url('assets/js/jquery.metisMenu.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.js" integrity="sha512-vUJTqeDCu0MKkOhuI83/MEX5HSNPW+Lw46BA775bAWIp1Zwgz3qggia/t2EnSGB9GoS2Ln6npDmbJTdNhHy1Yw==" crossorigin="anonymous"></script>
<!-- Custom Js -->
<script src="{{url('assets/js/custom-scripts.js')}}"></script>
<script src="{{url('assets/js/custom.js')}}"></script>


</body>
</html>
