@extends('layouts.delegate')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">
                {{$page_title}}
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <div class="panel panel-primary text-center no-boder">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger" id="errorAlert" style="display:none"></div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Select your picture</label>
                                <input type="file" name="" id="avatarSelect" class="form-control" accept="image/*">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Enter Access Code</label>
                                <input type="password" placeholder="Access code" id="access_code" class="form-control">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <button id="generateBadgeBtn" class="btn btn-primary"><i class="fa fa-id-badge"></i> Generate Badge</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-primary text-center no-boder">
                <div class="panel-body">
                    <div id="cropperDisplay"></div>
                </div>
            </div>
        </div>
    </div>

    @endsection