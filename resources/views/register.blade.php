<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="{{config('constants.dev')}}">
    <title>Register &lsaquo; {{config('constants.site')}}</title>


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="icon" href="{{url('assets/images/logo.png')}}" sizes="32x32" type="image/png">
    <link rel="icon" href="{{url('assets/images/logo.png')}}" sizes="16x16" type="image/png">
    <link rel="icon" href="{{url('assets/images/logo.png')}}">


    <style>

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
        html,
        body {
            height: 100%;
        }

        body {
            /*display: -ms-flexbox;*/
            /*display: flex;*/
            /*-ms-flex-align: center;*/
            /*align-items: center;*/
            /*padding-top: 40px;*/
            /*padding-bottom: 40px;*/
            background-color: #211e1c;
        }

        input, select {
            margin-top: 0;
        }

        .nav-tabs li a {
            background-color: black !important;
            padding: 8px;
            border-radius: 0;
            color: #ccc;
        }

        .nav-tabs li a.active {
            background: linear-gradient(#e74c3c 0%, #900 100%);
            padding: 8px;
            border-radius: 0;
            color: white !important;
        }

    </style>

</head>
<body class="text-center">

<div class="row">
    <div class="col-sm-12">
        @if(session('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
        @endif

            @if(session('message'))
                <div class="alert alert-primary">{{session('message')}}</div>
            @endif
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <img class="mb-4" src="{{url('assets/images/logo.png')}}" alt="" width="100">
        <h2 class="h5 mb-3 font-weight-normal text-warning">Please register below</h2>
    </div>

</div>
<div class="row">
    <div class="col-md-10 offset-md-1">
        <div class="card" style="background-color: #e0dfdf">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        Already have an account? <br>
                        <a href="{{route('login')}}" class="btn btn-danger btn-sm">Login Here</a>
                        <hr>
                    </div>
                </div>
                <form method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <label for=""><small><strong>Title</strong></small></label>
                                <select name="title" id="" class="form-control" required>
                                    <option value="">Select title</option>
                                    <option value="Pastor">Pastor</option>
                                    <option value="Deacon">Deacon</option>
                                    <option value="Deaconess">Deaconess</option>
                                    <option value="Brother">Brother</option>
                                    <option value="Sister">Sister</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <label for=""><small><strong>Firstname</strong></small></label>
                                <input type="text" class="form-control" name="firstname" value="{{old('firstname')}}" placeholder="Firstname" required>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <label for=""><small><strong>Lastname</strong></small></label>
                                <input type="text" class="form-control" name="lastname" value="{{old('lastname')}}" placeholder="Lastname" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <label for=""><small><strong>Zone</strong></small></label>
                                <select name="church_zone" id="" class="form-control" required>
                                    <option value="">Select your zone</option>
                                    @foreach($zones as $zone)
                                    <option value="{{$zone->id}}">{{$zone->zone_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <label for=""><small><strong>Group</strong></small></label>
                                <input type="text" class="form-control" name="church_group" placeholder="Group" required>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <label for=""><small><strong>Church</strong></small></label>
                                <input type="text" class="form-control" name="church" placeholder="Church" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-4 col-sm-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for=""><small><strong>KingsChat Number</strong></small></label>
                                        <input type="text" class="form-control phone" name="phone" placeholder="234xxxxxxxxxx" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for=""><small><strong>KingsChat ID</strong></small></label>
                                        <input type="text" class="form-control kc" name="kingschat" placeholder="@mykingschatID" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for=""><small><strong>Email Address</strong></small></label>
                                        <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-8 col-sm-8">
                            <label for="">&nbsp;</label>
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a data-toggle="tab" href="#principal" class="nav-link">I am a Principal</a>
                                </li>
                                <li class="nav-item">
                                    <a data-toggle="tab" href="#teacher" class="nav-link">I am a Teacher</a>
                                </li>
                            </ul>

                            <br>

                            <div class="tab-content">
                                <div class="tab-pane container active" id="defaultTab">
                                    <p class="text-center text-danger">Please select above <i class="icon-arrow-up"></i></p>
                                </div>
                                <div id="principal" class="tab-pane container fade">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""><small><strong>How long have you been a Foundation School Principal</strong></small></label>
                                                <select name="principal_duration" id="" class="form-control">
                                                    <option value="">Please select</option>
                                                    <option value="Less Than 1 Year">Less Than 1 Year</option>
                                                    <option value="1 - 5 Years">1 - 5 Years</option>
                                                    <option value="5 - 10 Years">5 - 10 Years</option>
                                                    <option value="Over 10 Years">Over 10 Years</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""><small><strong>How long have you been a Foundation School Teacher</strong></small></label>
                                                <select name="teacher_duration1" id="" class="form-control">
                                                    <option value="">Please select</option>
                                                    <option value="Less Than 1 Year">Less Than 1 Year</option>
                                                    <option value="1 - 5 Years">1 - 5 Years</option>
                                                    <option value="5 - 10 Years">5 - 10 Years</option>
                                                    <option value="Over 10 Years">Over 10 Years</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-6">
                                            <hr>
                                            <button type="submit" name="submit" value="principal" class="btn btn-outline-danger">Complete Registration <i class="icon-signin"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div id="teacher" class="tab-pane container fade">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for=""><small><strong>How did you become a Foundation School Teacher?</strong></small></label>
                                                <input type="text" class="form-control" name="teacher_process" placeholder="How did you become a Foundation school teacher?">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""><small><strong>Have you attended any formal Foundation School Teachers' Training Program?</strong></small></label>
                                                <select name="previous_attended" id="" class="form-control">
                                                    <option value="">Please select</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="no">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""><small><strong>How long have you been a Foundation School Teacher</strong></small></label>
                                                <select name="teacher_duration2" id="" class="form-control">
                                                    <option value="">Please select</option>
                                                    <option value="Less Than 1 Year">Less Than 1 Year</option>
                                                    <option value="1 - 5 Years">1 - 5 Years</option>
                                                    <option value="5 - 10 Years">5 - 10 Years</option>
                                                    <option value="Over 10 Years">Over 10 Years</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-6">
                                            <hr>
                                            <button type="submit" name="submit" value="teacher" class="btn btn-outline-danger">Complete Registration <i class="icon-signin"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha512-jCaU0Dp3IbMDlZ6f6dSEQSnOrSsugG6F6YigRWnagi7HoOLshF1kwxLT4+xCZRgQsTNqpUKj6WmWOxsu9l3URA==" crossorigin="anonymous"></script>

</body>
</html>