@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header bg-blue-gradient">
                    <h4 class="text-center">Weekly Reports</h4>
                </div>
                <div class="box-body">
                    <div class="row">

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="">Week</label>
                                <select id="reportWeek" class="form-control">
                                    <option value="">Select week...</option>
                                    <option value="1">Week 1</option>
                                    <option value="2">Week 2</option>
                                    <option value="3">Week 3</option>
                                    <option value="4">Week 4</option>
                                    <option value="5">Week 5</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="">Month</label>
                                <select name="" id="reportMonth" class="form-control">
                                    <option value="">Select month...</option>
                                    @foreach($months as $month)
                                    <option value="{{$month['val']}}">{{$month['title']}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="">Year</label>
                                <select name="" id="reportYear" class="form-control">
                                    <option value="">Select year...</option>
                                    @foreach($years as $year)
                                    <option value="{{$year}}">{{$year}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <button class="btn btn-app bg-blue-gradient showReportBtn">
                                <i class="fa fa-search"></i>
                                Show Report
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">

            <p id="instructionText" class="text-primary text-center">Select from the above fields</p>
            <div class="box" id="reportDisplay" style="display: none;">
                <div class="box-header bg-blue-gradient">
                    <h4 id="resultTitle" class="text-center"></h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <a id="downloadBtn" class="btn btn-app btn-danger"><i class="fa fa-download"></i> Download Report</a>
                        </div>
                    </div>
                    <div id="reportResult" class="table-responsive"></div>
                </div>
            </div>
        </div>
    </div>

    @endsection