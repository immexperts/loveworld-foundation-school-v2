@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-header bg-blue-gradient">
                            <h4 class="text-center">Principals</h4>
                        </div>
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Full Name</th>
                                            <th>Phone</th>
                                            <th>Zone</th>
                                            <th>Group</th>
                                            <th>Church</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($principals as $p)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$p->title . ' ' . $p->firstname . ' ' . $p->lastname}}</td>
                                            <td>{{$p->phone}}</td>
                                            <td>{{$p->zone_name}}</td>
                                            <td>{{$p->church_group}}</td>
                                            <td>{{$p->church}}</td>
                                        </tr>
                                        <?php $i++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    @endsection