@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-header bg-blue-gradient">
                            <h4 class="text-center"><i class="fa fa-book"></i> ALL RESOURCES</h4>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <button data-toggle="modal" data-target="#newResourceModal" class="btn btn-app bg-blue-gradient">
                                        <i class="fa fa-upload"></i>
                                        New Resource
                                    </button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th width="30px">#</th>
                                            <th>TITLE</th>
                                            <th>ACCESS</th>
                                            <th width="30px">FILE TYPE</th>
                                            <th width="40px">STATUS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $j = 1; ?>
                                        @foreach($resources as $resource)
                                        <tr>
                                            <td>{{$j}}</td>
                                            <td>{{$resource->title}}</td>
                                            <td>{{ucfirst($resource->access_level)}}</td>
                                            <td>{{strtoupper($resource->extension)}}</td>
                                            <td>
                                                <form method="post">
                                                    <button type="submit" name="delete_resource" value="<?=$resource['id'];?>" class="btn btn-xs btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php $j++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="newResourceModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center">New Resource</h4>
                </div>
                <form method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Resource Title *</label>
                                    <input type="text" class="form-control" name="title" placeholder="Resource Title" required>
                                </div>

                                <div class="form-group">
                                    <label for="">File</label>
                                    <input type="file" name="file" id="" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label for="">Who can download this file?</label>
                                    <select name="access_level" id="" class="form-control" required>
                                        <option value="">Select...</option>
                                        <option value="teacher">Teachers</option>
                                        <option value="principal">Principals</option>
                                        <option value="all">Everyone</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" name="create" value="create" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    @endsection