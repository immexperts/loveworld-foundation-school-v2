@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-header bg-blue-gradient">
                            <h4 class="text-center"><i class="fa fa-newspaper"></i> ALL ASSIGNMENTS</h4>
                        </div>
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th width="30px">#</th>
                                            <th>PRINCIPAL</th>
                                            <th class="text-center">ZONE</th>
                                            <th class="text-center" width="40px"><i class="fa fa-gear"></i></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $j = 1; ?>
                                        @foreach($assignments as $ass)
                                            <tr>
                                                <td>{{$j}}</td>
                                                <td>{{$ass->title}} {{$ass->firstname}} {{$ass->lastname}}</td>
                                                <td class="text-center">{{$ass->zone_name}}</td>
                                                <td><a href="{{route('downloadAssignment', $ass->id)}}" class="btn btn-sm btn-danger"><i class="fa fa-download"></i> Download</a></td>
                                            </tr>
                                            <?php $j++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @endsection