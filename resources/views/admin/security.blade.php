@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header bg-blue-gradient">
                    <h3 class="box-title"><i class="fa fa-key"></i> Password Change/Update</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <form method="post" action="{{route('changePassword')}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Enter current password</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                        <input type="password" class="form-control" name="old_password" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">New password</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                        <input type="password" class="form-control" name="password" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">Confirm new password</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                        <input type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-sm bg-blue-gradient" type="submit">Save Password</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="box">
                <div class="box-header bg-blue-gradient">
                    <h3 class="box-title"><i class="fa fa-money"></i> Access Code Setting</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <form method="post" action="{{route('changeAccessCode')}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Access Code</label>
                                    <input type="text" class="form-control" name="access_code" value="{{$setting->access_code}}" required>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-sm bg-blue-gradient pull-right"><i class="fa fa-money"></i> Save Access Code</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>
    @endsection