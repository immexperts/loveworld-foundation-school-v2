@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-header bg-blue-gradient">
                            <h4 class="text-center">Teachers</h4>
                        </div>
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Full Name</th>
                                            <th>Phone</th>
                                            <th>Zone</th>
                                            <th>Group</th>
                                            <th>Church</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $j = 1;?>
                                        @foreach($teachers as $t)
                                        <tr>
                                            <td>{{$j}}</td>
                                            <td>{{$t->title . ' ' . $t->firstname . ' ' . $t->lastname}}</td>
                                            <td>{{$t->phone}}</td>
                                            <td>{{$t->zone_name}}</td>
                                            <td>{{$t->church_group}}</td>
                                            <td>{{$t->church}}</td>
                                        </tr>
                                        <?php $j++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    @endsection