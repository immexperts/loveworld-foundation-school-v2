@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-sm-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue-gradient">
                <div class="inner">
                    <h3>{{$principals}}</h3>

                    <p>Principals</p>
                </div>
                <div class="icon">
                    <i class="fa fa-star"></i>
                </div>

            </div>
        </div>

        <div class="col-sm-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue-gradient">
                <div class="inner">
                    <h3>{{$teachers}}</h3>

                    <p>Teachers</p>
                </div>
                <div class="icon">
                    <i class="fa fa-star"></i>
                </div>

            </div>
        </div>

    </div>

    @endsection