@extends('layouts.delegate')

@section('content')

    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<h1 class="page-header">--}}
                {{--Empty Page <small>Create new page.</small>--}}
            {{--</h1>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="row">
        <div class="col-sm-6 col-md-6">
            <div class="panel panel-primary text-center no-boder">
                <div class="panel-body">
                    <i class="fa fa-file fa-5x"></i>
                    <h3>Assignments</h3>
                </div>
                <a href="{{route('assignments')}}">
                    <div class="panel-footer back-footer-blue">
                        Click to submit assignment
                    </div>
                </a>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="panel panel-primary text-center no-boder">
                <div class="panel-body">
                    <i class="fa fa-book fa-5x"></i>
                    <h3>Resources </h3>
                </div>
                <a href="{{route('viewResources')}}"><div class="panel-footer back-footer-blue">
                    View
                    </div></a>
            </div>
        </div>
    </div>
    @endsection