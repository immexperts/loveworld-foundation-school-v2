<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="{{config('constants.dev')}}">
    <title>Log In &lsaquo; {{config('constants.site')}}</title>


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="icon" href="{{url('assets/images/favicon.png')}}" sizes="32x32" type="image/png">
    <link rel="icon" href="{{url('assets/images/favicon.png')}}" sizes="16x16" type="image/png">
    <link rel="icon" href="{{url('assets/images/favicon.png')}}">
    <meta name="theme-color" content="#563d7c">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
        html,
        body {
            height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #151134;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: auto;
        }
        .form-signin .checkbox {
            font-weight: 400;
        }
        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>

</head>
<body class="text-center">
<form class="form-signin" method="post">
    @csrf
    <img class="mb-4" src="{{url('assets/images/logo.png')}}" alt="" width="150">
    <h1 class="h3 mb-3 font-weight-normal text-white">Welcome Delegate</h1>

    @if(session('error'))
    <div class="alert alert-danger">{{session('error')}}</div>
    @endif

    <label for="inputEmail" class="text-warning">Please enter your email address to continue</label>
    <input type="text" class="form-control" name="email" placeholder="Email address" required autofocus>
    <div class="checkbox mb-3">

    </div>
    <button class="btn btn-lg btn-warning btn-block" type="submit" name="login" value="login">Log In!</button>

    <p class="text-white mt-5">Are you new here?</p>
    <a href="{{route('register')}}" class="btn btn-primary btn-lg">Signup</a>

    <p class="mt-5 mb-3 text-muted">&copy; {{date('Y')}} <a href="{{route('home')}}">{{config('constants.dev')}}</a></p>
</form>
</body>
</html>