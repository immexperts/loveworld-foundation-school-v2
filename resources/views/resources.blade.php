@extends('layouts.delegate')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">
            {{$page_title}}
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-book"></i> Download Resources
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        @foreach($resources as $resource)
                        <li class="list-group-item">
                            <i class="fa fa-book"></i> {{$resource->title}} | {{strtoupper($resource->extension)}}
                            <a href="{{route('downloadResource', $resource->id)}}" class="btn btn-xs btn-primary pull-right"><i class="fa fa-download"></i> Download</a>
                            <br>
                        </li>
                            <br>
                            @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endsection