@extends('layouts.delegate')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">
            {{$page_title}}
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-file"></i> Submit Assignment Here
                </div>
                <div class="panel-body">

                    <form method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <div class="form-group">
                                    <label for="">Select document to upload</label>
                                    <input type="file" name="document" id="" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-sm btn-primary pull-right"><i class="fa fa-upload"></i> Upload</button>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection