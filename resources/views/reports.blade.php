@extends('layouts.delegate')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">
                {{$page_title}}
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <button data-toggle="modal" data-target="#reportModal" class="btn btn-primary"><i class="fa fa-pencil"></i> Submit Report</button>
            <hr>
        </div>

        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-pencil"></i> Your Reports
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>TITLE</th>
                                <th>NO IN FSCH</th>
                                <th>ATTENDANCE</th>
                                <th>ZONAL CAPACITY</th>
                                <th>CLASS 1</th>
                                <th>CLASS 2</th>
                                <th>CLASS 3</th>
                                <th>CLASS 4</th>
                                <th>CLASS 5</th>
                                <th>CLASS 6</th>
                                <th>CLASS 7</th>
                                <th>ELIGIBLE FOR GRADUATION</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1;?>
                        @foreach($reports as $report)
                            <tr>
                                <td>{{$i}}</td>
                                <td>Week {{$report->week}} {{gmdate("F", strtotime("2021-$report->month-10"))}} {{$report->year}}</td>
                                <td>{{$report->fs_number}}</td>
                                <td>{{$report->week_att}}</td>
                                <td>{{$report->fs_capacity}}</td>
                                <td>{{$report->class_1}}</td>
                                <td>{{$report->class_2}}</td>
                                <td>{{$report->class_3}}</td>
                                <td>{{$report->class_4}}</td>
                                <td>{{$report->class_5}}</td>
                                <td>{{$report->class_6}}</td>
                                <td>{{$report->class_7}}</td>
                                <td>{{$report->eligible_grad}}</td>
                            </tr>
                            <?php $i++;?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form method="post">
                @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">New Report</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Week</label>
                                <select name="week" class="form-control" required>
                                    <option value="">Select week...</option>
                                    <option value="1">Week 1</option>
                                    <option value="2">Week 2</option>
                                    <option value="3">Week 3</option>
                                    <option value="4">Week 4</option>
                                    <option value="5">Week 5</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Month</label>
                                <select name="month" class="form-control" required>
                                    <option value="">Select month...</option>
                                    @foreach($months as $month)
                                        <option value="{{$month['val']}}">{{$month['title']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Year</label>
                                <select name="year" class="form-control" required>
                                    <option value="">Select year...</option>
                                    @foreach($years as $year)
                                        <option value="{{$year}}">{{$year}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for=""> Total No in F Sch.</label>
                                <input type="number" name="fs_number" min="0" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for=""> Total F Sch. zonal class capacity</label>
                                <input type="number" name="fs_capacity" min="0" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for=""> Week Attendance</label>
                                <input type="number" name="week_att" min="0" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for=""> Class 1</label>
                                <input type="number" name="class_1" min="0" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for=""> Class 2</label>
                                <input type="number" name="class_2" min="0" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for=""> Class 3</label>
                                <input type="number" name="class_3" min="0" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for=""> Class 4</label>
                                <input type="number" name="class_4" min="0" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for=""> Class 5</label>
                                <input type="number" name="class_5" min="0" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for=""> Class 6</label>
                                <input type="number" name="class_6" min="0" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for=""> Class 7</label>
                                <input type="number" name="class_7" min="0" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for=""> Eligible Graduating</label>
                                <input type="number" name="eligible_grad" min="0" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Report</button>
                </div>
            </div>
            </form>
        </div>
    </div>
    @endsection