-- Adminer 4.7.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `assignments`;
CREATE TABLE `assignments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `total_questions` int(11) DEFAULT '0',
  `is_active` enum('1','0') DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `catchup`;
CREATE TABLE `catchup` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delegate_id` int(11) DEFAULT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `delegates`;
CREATE TABLE `delegates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `church_group` varchar(255) DEFAULT NULL,
  `church` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `kcid` varchar(255) DEFAULT NULL,
  `badge_code` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `role` enum('principal','teacher') DEFAULT NULL,
  `teacher_duration` varchar(255) DEFAULT NULL COMMENT 'How long have you been a teacher?',
  `principal_duration` varchar(255) DEFAULT NULL COMMENT 'How long have you been a principal?',
  `teacher_process` text COMMENT 'How did you become a foundation school teacher?',
  `previous_attended` enum('yes','no') DEFAULT NULL COMMENT 'Have you attended any formal foundation school teachers training before?',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `downloaded_res`;
CREATE TABLE `downloaded_res` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delegate_id` int(11) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `dummytbl`;
CREATE TABLE `dummytbl` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delegate_id` int(11) DEFAULT NULL,
  `attMonth` int(2) DEFAULT NULL,
  `attDay` int(2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `week` int(11) DEFAULT NULL,
  `delegate_id` int(11) DEFAULT NULL,
  `fs_number` int(11) DEFAULT NULL COMMENT 'Total number in foundation school',
  `fs_capacity` int(11) DEFAULT '0',
  `week_att` int(11) DEFAULT NULL,
  `class_1` int(11) DEFAULT NULL,
  `class_2` int(11) DEFAULT NULL,
  `class_3` int(11) DEFAULT NULL,
  `class_4` int(11) DEFAULT NULL,
  `class_5` int(11) DEFAULT NULL,
  `class_6` int(11) DEFAULT NULL,
  `class_7` int(11) DEFAULT '0',
  `eligible_grad` int(11) DEFAULT NULL COMMENT 'Total eligible for graduation',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `report_dates`;
CREATE TABLE `report_dates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `month` int(2) DEFAULT NULL,
  `week` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `resources`;
CREATE TABLE `resources` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `access_level` enum('teacher','principal','all') DEFAULT 'all',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stream` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `access` text,
  `access_code` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `settings` (`id`, `stream`, `username`, `password`, `access`, `access_code`, `created_at`, `updated_at`) VALUES
(1,	'https://vcpout-sf01.internetmultimediaonline.org/vcp/lwfs2021xd/playlist.m3u8',	'admin',	'$2y$10$.on6.8wcIlySP/pj7QfD1uUBrBV57dspCcqxT98ALwuIFnVTxfyoq',	'principal,teacher',	'H56349',	NULL,	'2021-03-04 15:58:04');

DROP TABLE IF EXISTS `submissions`;
CREATE TABLE `submissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delegate_id` int(11) DEFAULT NULL,
  `assignment_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `submitted`;
CREATE TABLE `submitted` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delegate_id` int(11) DEFAULT NULL,
  `assignment_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `temp_submissions`;
CREATE TABLE `temp_submissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delegate_id` int(11) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `zones`;
CREATE TABLE `zones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `zones` (`id`, `zone_name`, `created_at`, `updated_at`) VALUES
(6,	'Ministry Centre Abuja',	'2019-07-17 18:42:31',	'2019-07-17 18:42:31'),
(7,	'Ministry Centre | Port Harcourt Ministry Centre',	'2019-07-17 18:42:31',	'2019-07-17 18:42:31'),
(8,	'Ministry Centre | Warri Ministry Centre',	'2019-07-17 18:42:31',	'2019-07-17 18:42:31'),
(9,	'Ministry Centre Abeokuta',	'2019-07-17 18:42:32',	'2019-07-17 18:42:32'),
(10,	'Ministry Centre | Calabar Ministry Centre',	'2019-07-17 18:42:32',	'2019-07-17 18:42:32'),
(11,	'Eastern Europe Virtual Region | Eastern Europe Region',	'2019-07-17 18:42:32',	'2019-07-17 18:42:32'),
(12,	'South America Virtual Region | South America Region',	'2019-07-17 18:42:33',	'2019-07-17 18:42:33'),
(13,	'Middle East and South East Asia Virtual Region | Middle East & South East Asia Region',	'2019-07-17 18:42:33',	'2019-07-17 18:42:33'),
(14,	'DSP Region | Texas Zone 1 (USA Region 3)',	'2019-07-17 18:42:33',	'2019-07-17 18:42:33'),
(15,	'DSP Region | Texas Zone 2 (USA Region 3)',	'2019-07-17 18:42:34',	'2019-07-17 18:42:34'),
(16,	'DSP Region | UK Zone 1 (UK Region 1)',	'2019-07-17 18:42:34',	'2019-07-17 18:42:34'),
(17,	'DSP Region | UK Zone 2 (UK Region 1)',	'2019-07-17 18:42:34',	'2019-07-17 18:42:34'),
(18,	'DSP Region | UK Zone 3 (UK Region 1)',	'2019-07-17 18:42:34',	'2019-07-17 18:42:34'),
(19,	'DSP Region | UK Zone 4 (UK Region 1)',	'2019-07-17 18:42:35',	'2019-07-17 18:42:35'),
(20,	'DSP Region | SA Zone 1',	'2019-07-17 18:42:35',	'2019-07-17 18:42:35'),
(21,	'DSP Region | SA Zone 5',	'2019-07-17 18:42:35',	'2019-07-17 18:42:35'),
(22,	'DSP Region | Kenya Zone',	'2019-07-17 18:42:36',	'2019-07-17 18:42:36'),
(23,	'Accra Zone',	'2019-07-17 18:42:36',	'2019-07-17 18:42:36'),
(24,	'DSP Region | Onitsha Zone',	'2019-07-17 18:42:36',	'2019-07-17 18:42:36'),
(25,	'DSP Region | Port Harcourt Zone 1',	'2019-07-17 18:42:37',	'2019-07-17 18:42:37'),
(26,	'DSP Region | Port Harcourt Zone 2',	'2019-07-17 18:42:37',	'2019-07-17 18:42:37'),
(27,	'DSP Region | Port Harcourt Zone 3',	'2019-07-17 18:42:37',	'2019-07-17 18:42:37'),
(28,	'DSP Region | Benin Zone 1',	'2019-07-17 18:42:37',	'2019-07-17 18:42:37'),
(29,	'DSP Region | Abuja Zone',	'2019-07-17 18:42:38',	'2019-07-17 18:42:38'),
(30,	'DSP Region | Midwest Zone',	'2019-07-17 18:42:38',	'2019-07-17 18:42:38'),
(31,	'USA Region 1 | Zone 1',	'2019-07-17 18:42:38',	'2019-07-17 18:42:38'),
(32,	'USA Region 1 | Zone 2',	'2019-07-17 18:42:39',	'2019-07-17 18:42:39'),
(33,	'USA Region 2 | Dallas and Atlanta Groups',	'2019-07-17 18:42:39',	'2019-07-17 18:42:39'),
(34,	'CE Chad',	'2019-07-17 18:42:39',	'2019-07-17 18:42:39'),
(35,	'UK Region 2 | UK Zone 1',	'2019-07-17 18:42:40',	'2019-07-17 18:42:40'),
(36,	'UK Region 2 | UK Zone 3',	'2019-07-17 18:42:40',	'2019-07-17 18:42:40'),
(37,	'UK Region 2 | UK Zone 4',	'2019-07-17 18:42:40',	'2019-07-17 18:42:40'),
(38,	'Western Europe Region | Western Europe Zone 4',	'2019-07-17 18:42:41',	'2019-07-17 18:42:41'),
(39,	'Western Europe Region | Western Europe Zone 1',	'2019-07-17 18:42:42',	'2019-07-17 18:42:42'),
(40,	'Western Europe Region | Western Europe Zone 2',	'2019-07-17 18:42:42',	'2019-07-17 18:42:42'),
(41,	'Western Europe Region | Western Europe Zone 3',	'2019-07-17 18:42:43',	'2019-07-17 18:42:43'),
(42,	'Canada and Central America Region | Canada Zone',	'2019-07-17 18:42:43',	'2019-07-17 18:42:43'),
(43,	'Canada and Central America Region | Quebec Zone',	'2019-07-17 18:42:43',	'2019-07-17 18:42:43'),
(44,	'| Australia Zone',	'2019-07-17 18:42:44',	'2019-07-17 18:42:44'),
(45,	'Southern Africa Region | SA Zone 2',	'2019-07-17 18:42:44',	'2019-07-17 18:42:44'),
(46,	'Southern Africa Region | SA Zone 3',	'2019-07-17 18:42:44',	'2019-07-17 18:42:44'),
(47,	'Southern Africa Region | SA Zone 4',	'2019-07-17 18:42:45',	'2019-07-17 18:42:45'),
(48,	'East, West and Central Africa Region | EWC Zone 1',	'2019-07-17 18:42:45',	'2019-07-17 18:42:45'),
(49,	'East, West and Central Africa Region | EWC Zone 2',	'2019-07-17 18:42:45',	'2019-07-17 18:42:45'),
(50,	'East, West and Central Africa Region | EWC Zone 3',	'2019-07-17 18:42:46',	'2019-07-17 18:42:46'),
(51,	'East, West and Central Africa Region | EWC Zone 4',	'2019-07-17 18:42:46',	'2019-07-17 18:42:46'),
(52,	'East, West and Central Africa Region | EWC Zone 5',	'2019-07-17 18:42:46',	'2019-07-17 18:42:46'),
(53,	'East, West and Central Africa Region | EWC Zone 6',	'2019-07-17 18:42:47',	'2019-07-17 18:42:47'),
(54,	'Northern Region | NW Zone 1',	'2019-07-17 18:42:47',	'2019-07-17 18:42:47'),
(55,	'Northern Region | NW Zone 2',	'2019-07-17 18:42:48',	'2019-07-17 18:42:48'),
(56,	'Northern Region | NC Zone 1',	'2019-07-17 18:42:49',	'2019-07-17 18:42:49'),
(57,	'Northern Region | NC Zone 2',	'2019-07-17 18:42:49',	'2019-07-17 18:42:49'),
(58,	'Northern Region | NE Zone 1',	'2019-07-17 18:42:50',	'2019-07-17 18:42:50'),
(59,	'Nigeria South East Region | SE Zone 1',	'2019-07-17 18:42:50',	'2019-07-17 18:42:50'),
(60,	'Nigeria South East Region | SE Zone 2',	'2019-07-17 18:42:51',	'2019-07-17 18:42:51'),
(61,	'Nigeria South East Region | Aba Zone',	'2019-07-17 18:42:51',	'2019-07-17 18:42:51'),
(62,	'Nigeria South East Region | Benin Zone 2',	'2019-07-17 18:42:51',	'2019-07-17 18:42:51'),
(63,	'Nigeria South East Region | Edo North and Central Zone',	'2019-07-17 18:42:52',	'2019-07-17 18:42:52'),
(64,	'Nigeria South South Region | NSS Zone 1',	'2019-07-17 18:42:53',	'2019-07-17 18:42:53'),
(65,	'Nigeria South South Region | NSS Zone 2',	'2019-07-17 18:42:53',	'2019-07-17 18:42:53'),
(66,	'Nigeria South West Region | NSW Zone 1',	'2019-07-17 18:42:54',	'2019-07-17 18:42:54'),
(67,	'Nigeria South West Region | NSW Zone 2',	'2019-07-17 18:42:55',	'2019-07-17 18:42:55'),
(68,	'Nigeria South West Region | South-West Zone 3.',	'2019-07-17 18:42:56',	'2019-07-17 18:42:56'),
(70,	'Lagos Zone 1',	'2021-01-06 11:04:47',	'2021-01-06 11:04:47'),
(71,	'Lagos Zone 2',	'2021-01-06 11:05:13',	'2021-01-06 11:05:13'),
(72,	'Lagos Zone 3',	'2021-01-06 11:05:28',	'2021-01-06 11:05:28'),
(73,	'Lagos Zone 4',	'2021-01-06 11:05:41',	'2021-01-06 11:05:41'),
(74,	'Lagos Zone 5',	'2021-01-06 11:05:57',	'2021-01-06 11:05:57'),
(75,	'Lagos Virtual Zone',	'2021-01-06 11:06:14',	'2021-01-06 11:06:14');

-- 2021-03-19 10:18:10
