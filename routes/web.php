<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', [AuthController::class, 'showDelegateLogin'])->name('login');
Route::get('admin-login', [AuthController::class, 'showAdminLogin'])->name('adminLogin');

Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::post('login', [AuthController::class, 'delegateLogin']);
Route::post('admin-login', [AuthController::class, 'adminLogin']);

Route::get('register', [AuthController::class, 'showRegister'])->name('register');
Route::post('register', [AuthController::class, 'delegateRegister']);

Route::group(['middleware' => 'isDelegate'], function(){
    Route::get('/', [WebController::class, 'home'])->name('home');
    Route::get('reports', [WebController::class, 'reports'])->name('reports');
    Route::post('reports', [WebController::class, 'submitReport']);
    Route::get('badge', [WebController::class, 'badge'])->name('badge');
    Route::get('assignments', [WebController::class, 'assignments'])->name('assignments');
    Route::post('assignments', [WebController::class, 'uploadAssignment']);
    Route::post('badge', [WebController::class, 'generateBadge']);
    Route::get('download_badge', [WebController::class, 'downloadBadge']);
    Route::get('resources', [WebController::class, 'resources'])->name('viewResources');
    Route::get('download_resource/{id}', [WebController::class, 'downloadResource'])->name('downloadResource');
});

Route::group(['middleware' => 'isAdmin', 'prefix' => 'admin'], function(){
    Route::get('/', [AdminController::class, 'dashboard'])->name('adminHome');
    Route::get('principals', [AdminController::class, 'principals'])->name('principals');
    Route::get('teachers', [AdminController::class, 'teachers'])->name('teachers');
    Route::get('assignments', [AdminController::class, 'assignments'])->name('adminAssignments');
    Route::get('assignment/download/{id}', [AdminController::class, 'downloadAssignment'])->name('downloadAssignment');

    Route::get('resources', [AdminController::class, 'resources'])->name('resources');
    Route::post('resources', [AdminController::class, 'uploadResource']);

    Route::get('reports', [AdminController::class, 'reports'])->name('adminReports');
    Route::post('reports/fetch', [AdminController::class, 'fetchReports']);

    Route::get('reports/download/{year}/{month}/{week}', [AdminController::class, 'downloadReports'])->name('downloadReports');
    Route::get('security', [AdminController::class, 'securityPage'])->name('security');
    Route::post('changecode', [AdminController::class, 'changeAccessCode'])->name('changeAccessCode');
    Route::post('changepassword', [AdminController::class, 'changePassword'])->name('changePassword');
});
