const loc = (location.host === 'lwfs.test') ? 'http://lwfs.test/' : 'https://'+location.host+'/';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


const avatarSelect = $('#avatarSelect');
const cropperDisplay = $('#cropperDisplay');
const genBtn = $('#generateBadgeBtn');
const errorAlert = $('#errorAlert');

genBtn.attr("disabled", true);
cropperDisplay.croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'circle'
    },
    boundary: {
        width: 250,
        height: 250
    }
});

avatarSelect.on('change', function(e){
    errorAlert.hide();
    var reader = new FileReader();
    const file = e.target.files[0];

    var types = ["image/jpeg", "image/jpg", "image/png"];

    if(types.includes(file.type)){

        genBtn.attr("disabled", false);


        reader.addEventListener("load", function(e){
            cropperDisplay.croppie('bind', {
                url: e.target.result
            }).then(function(){
            });
        });

        reader.readAsDataURL(file);
    } else {
        genBtn.attr("disabled", true);
    }

});

genBtn.on('click', function(){
    errorAlert.hide();
    const code = $('#access_code').val().trim();
    if(code.length){
        genBtn.html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
        genBtn.attr("disabled", true);
        cropperDisplay.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function(resp){
            $.ajax({
                url: loc + "badge",
                type: "POST",
                data: {"image": resp, "code": code},
                success: function(data){
                    if(data.status === true){
                        location.href = loc + 'download_badge';
                    } else {
                        genBtn.attr("disabled", false);
                        genBtn.html('<i class="fa fa-id-badge"></i> Generate Badge');
                        errorAlert.html(data.message);
                        errorAlert.show();
                    }
                },
                error: function(){
                    genBtn.attr("disabled", false);
                    genBtn.html('<i class="fa fa-id-badge"></i> Generate Badge');
                    errorAlert.html('Please try again');
                    errorAlert.show();
                }
            });
        })
    }
});
