const loc = (location.host === 'lwfs.test') ? 'http://lwfs.test/' : 'https://'+location.host+'/';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



$('.sidebar-menu').tree();

$('.date').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-0d',
    clearBtn: true
});

$('.datetime').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
    showClose: true,
    showClear: true,
    minDate: new Date(),
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down",
        previous: "fa fa-chevron-left",
        next: "fa fa-chevron-right",
        today: "fa fa-clock-o",
        clear: "fa fa-trash-o",
        close: "fa fa-check"
    }
});

$('.table').DataTable();

$('.phone').mask('+999999999999999');
$('.accountNumber').mask('9999999999');

$('.showReportBtn').on('click', function(){
    const year = $('#reportYear').val();
    const month = $('#reportMonth').val();
    const week = $('#reportWeek').val();
    const resultDiv = $('#reportResult');
    const instText = $('#instructionText');
    const resDisplay = $('#reportDisplay');

    if(year !== "" && month !== "" && week !== ""){
        resDisplay.hide();
        instText.html('<i class="fa fa-spinner fa-spin"></i> Fetching reports...');
        instText.show();

        $.ajax({
            method: "post",
            url: loc + "admin/reports/fetch",
            data: {year: year, month: month, week: week},
            success: function(data){
                if(data.status === true){
                    resDisplay.show();
                    instText.hide();
                    var html = '<table class="table table-striped">';
                    html = html.concat('<thead>');
                    html = html.concat('<tr>');

                    html = html.concat('<th>#</th>');
                    html = html.concat('<th>PRINCIPAL</th>');
                    html = html.concat('<th>ZONE</th>');
                    html = html.concat('<th>NO IN FSCH</th>');
                    html = html.concat('<th>ATTENDANCE</th>');
                    html = html.concat('<th>ZONAL CAPACITY</th>');
                    html = html.concat('<th>CLASS 1</th>');
                    html = html.concat('<th>CLASS 2</th>');
                    html = html.concat('<th>CLASS 3</th>');
                    html = html.concat('<th>CLASS 4</th>');
                    html = html.concat('<th>CLASS 5</th>');
                    html = html.concat('<th>CLASS 6</th>');
                    html = html.concat('<th>CLASS 7</th>');
                    html = html.concat('<th>ELIGIBLE FOR GRADUATION</th>');
                    html = html.concat('</tr>');
                    html = html.concat('</thead>');
                    html = html.concat('<tbody>');

                    for(var i = 0; i < data.data.report.length; i++){
                        const resp = data.data.report;
                        var j = i + 1;
                        html = html.concat('<tr>');
                        html = html.concat('<td>' + j + '</td>');
                        html = html.concat('<td>' + resp[i].title + ' ' + resp[i].firstname + ' ' + resp[i].lastname + '</td>');
                        html = html.concat('<td>' + resp[i].zone_name + '</td>');
                        html = html.concat('<td>' + resp[i].fs_number + '</td>');
                        html = html.concat('<td>' + resp[i].week_att + '</td>');
                        html = html.concat('<td>' + resp[i].fs_capacity + '</td>');
                        html = html.concat('<td>' + resp[i].class_1 + '</td>');
                        html = html.concat('<td>' + resp[i].class_2 + '</td>');
                        html = html.concat('<td>' + resp[i].class_3 + '</td>');
                        html = html.concat('<td>' + resp[i].class_4 + '</td>');
                        html = html.concat('<td>' + resp[i].class_5 + '</td>');
                        html = html.concat('<td>' + resp[i].class_6 + '</td>');
                        html = html.concat('<td>' + resp[i].class_7 + '</td>');
                        html = html.concat('<td>' + resp[i].eligible_grad + '</td>');
                        html = html.concat('</tr>');
                    }

                    html = html.concat('</tbody>');
                    resultDiv.html(html);
                    $('#downloadBtn').attr('href', data.data.link);
                    $('#resultTitle').html(data.data.title);
                }
            }
        });
    }
});
