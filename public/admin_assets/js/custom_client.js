const loc = (location.host === 'skr.test') ? 'http://skr.test/' : 'http://'+location.host+'/';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.sidebar-menu').tree();

$('.date').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-0d',
    clearBtn: true
});

$('.datetime').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
    showClose: true,
    showClear: true,
    minDate: new Date(),
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down",
        previous: "fa fa-chevron-left",
        next: "fa fa-chevron-right",
        today: "fa fa-clock-o",
        clear: "fa fa-trash-o",
        close: "fa fa-check"
    }
});

$('.table').DataTable();

$('.phone').mask('+999999999999999');
$('.accountNumber').mask('9999999999');

const errorAlert = $('#errorAlert');

$('.paymentBtn').on('click', function(e){
    e.preventDefault();
    const obj       = {};
    obj.amount      = $(this).attr('data-price');
    obj.currency    = $(this).attr('data-currency');
    obj.key         = $(this).attr('data-key');
    obj.track       = $(this).attr('data-track');
    obj.email       = $(this).attr('data-email');
    makePayment(obj);

});

$('.albumPaymentBtn').on('click', function(e){
    e.preventDefault();
    const obj       = {};
    obj.amount      = $(this).attr('data-price');
    obj.currency    = $(this).attr('data-currency');
    obj.key         = $(this).attr('data-key');
    obj.album       = $(this).attr('data-album');
    obj.email       = $(this).attr('data-email');
    makePayment(obj);

});

function makePayment(obj){
    errorAlert.hide();
    var handler = PaystackPop.setup({
        key: obj.key,
        email: obj.email,
        amount: obj.amount * 100,
        currency: obj.currency,
        callback: function(response){
            obj.reference = response.reference;
            verifyPayment(obj);
        },
        onClose: function(){

        }
    });
    handler.openIframe();
}

function verifyPayment(obj){
    const url = $('#paymentVerifyLink').val();
    $.ajax({
        method: "post",
        url: url,
        data: obj,
        success: function(data){
            console.log(data);
            if(data.status === true){
                location.reload(true);
            }
        },
        error: function(err){
            console.log(err);
            // errorAlert.html(err.error());
            errorAlert.show();
        }
    });
}
