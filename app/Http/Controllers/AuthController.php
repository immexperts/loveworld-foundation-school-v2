<?php

namespace App\Http\Controllers;

use App\Models\Delegate;
use App\Models\Setting;
use App\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function delegateLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        $user = Delegate::where('email', $request->email)->first();
        if($user){
            session()->put('delegate', $user);
            return redirect()->route('home');
        } else {
            return redirect()->back()->with('error', 'User does not exist');
        }
    }

    public function adminLogin(Request $request)
    {
        $request->validate([
            'username'     => 'required',
            'password'  => 'required'
        ]);

        $user = Setting::where('username', $request->username)->first();
        if($user){
            $verified = password_verify($request->password, $user->password);
            if($verified){
                session()->put('admin', $user);
                return redirect()->route('adminHome');
            } else {
                return redirect()->back()->with('error', 'Password does not match');
            }
        } else {
            return redirect()->back()->with('error', 'User not found');
        }

    }

    public function showDelegateLogin()
    {
        return view('login');
    }

    public function showRegister()
    {
        $data['zones'] = Zone::all();
        return view('register', $data);
    }

    public function delegateRegister(Request $request)
    {
        $request->validate([
            'title'         => 'required',
            'firstname'     => 'required',
            'lastname'      => 'required',
            'church_zone'   => 'required',
            'church_group'  => 'required',
            'church'        => 'required',
            'phone'         => 'required',
            'email'         => 'required|email',
            'kingschat'     => 'required'
        ]);

        $exists = Delegate::where('email', $request->email)->exists();

        if($exists){
            return redirect()->back()->with('error', 'You have registered previously.');
        } else {

            $reg = new Delegate();

            if($request->submit == 'principal'){
                if($request->has('principal_duration') && $request->has('teacher_duration1')){
                    $reg->principal_duration = $request->principal_duration;
                    $reg->teacher_duration = $request->teacher_duration1;

                } else {
                    return redirect()->back()->with('error', 'Please fill the form completely.');
                }
            }else if($request->submit == 'teacher'){
                if($request->has('teacher_duration2') && $request->has('teacher_process') && $request->has('previous_attended')){
                    $reg->teacher_process = $request->teacher_process;
                    $reg->teacher_duration = $request->teacher_duration2;
                    $reg->previous_attended = $request->previous_attended;

                } else {
                    return redirect()->back()->with('error', 'Please fill the form completely.');
                }
            } else {
                abort(403);
                return false;
            }

            $reg->title = $request->title;
            $reg->firstname = Str::title($request->firstname);
            $reg->lastname = Str::title($request->lastname);
            $reg->zone_id = $request->church_zone;
            $reg->church_group = $request->church_group;
            $reg->church = $request->church;
            $reg->email = $request->email;
            $reg->kcid = $request->kingschat;
            $reg->role = $request->submit;

            $reg->save();
            return redirect()->back()->with('message', 'You have completed your registration.');

        }

    }

    public function showAdminLogin()
    {
        return view('admin.login');
    }

    public function logout()
    {
        session()->flush();
        return redirect()->route('login');
    }
}
