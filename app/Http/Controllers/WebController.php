<?php

namespace App\Http\Controllers;

use App\Models\Delegate;
use App\Models\DownloadedRes;
use App\Models\Report;
use App\Models\ReportDate;
use App\Models\Resource;
use App\Models\Setting;
use App\Models\TempSubmit;
use Fpdf\Fpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class WebController extends Controller
{
    public function home()
    {
        $data['page_title'] = 'Home';
        $data['home_menu'] = true;
        return view('home', $data);
    }

    public function reports()
    {
        $delegate = Session::get('delegate');
        if($delegate->role == 'principal'){
            $months = [];
            $years = [];
            for($i = 1; $i <= 12; $i++){
                $dateObj   = \DateTime::createFromFormat('!m', $i);
                $monthName = $dateObj->format('F');
                array_push($months, [
                    'val' => $i,
                    'title' => $monthName
                ]);
            }

            if(date('Y') == 2021){
                array_push($years, 2021);
            } else {
                for($y = date('Y'); $y >= 2021; $y--){
                    array_push($years, $y);
                }
            }

            $data['years'] = $years;
            $data['months'] = $months;

            $reports = Report::where('delegate_id', $delegate->id)->get();

            $data['reports'] = $reports;
            $data['page_title'] = 'Weekly Reports';
            $data['report_menu'] = true;

            return view('reports', $data);
        } else {
            return redirect()->back();
        }
    }

    public function submitReport(Request $request)
    {
        $request->validate([
            'week'          => 'required',
            'month'         => 'required',
            'year'          => 'required',
            'fs_number'     => 'required',
            'week_att'      => 'required',
            'class_1'       => 'required',
            'class_2'       => 'required',
            'class_3'       => 'required',
            'class_4'       => 'required',
            'class_5'       => 'required',
            'class_6'       => 'required',
            'eligible_grad' => 'required'
        ]);

        $delegate = Session::get('delegate');
        if($delegate->role == 'principal'){
            $exists = Report::where('year', $request->year)->where('month', $request->month)
                ->where('week', $request->week)->where('delegate_id', $delegate->id)->exists();

            if($exists){
                return redirect()->back()->with('error', 'Report submitted already');
            } else {
                $report = new Report();
                $report->year = $request->year;
                $report->month = $request->month;
                $report->week = $request->week;
                $report->delegate_id = $delegate->id;
                $report->fs_number = $request->fs_number;
                $report->fs_capacity = $request->fs_capacity;
                $report->week_att = $request->week_att;
                $report->class_1 = $request->class_1;
                $report->class_2 = $request->class_2;
                $report->class_3 = $request->class_3;
                $report->class_4 = $request->class_4;
                $report->class_5 = $request->class_5;
                $report->class_6 = $request->class_6;
                $report->class_7 = $request->class_7;
                $report->eligible_grad = $request->eligible_grad;

                $report->save();
                return redirect()->back()->with('message', 'Report submitted successfully');

            }
        } else {
            return redirect()->back();
        }
    }

    public function badge()
    {
        $data['page_title'] = 'Access Badge';
        $data['badge_menu'] = true;
        return view('badge', $data);
    }

    public function generateBadge(Request $request)
    {
        $request->validate([
            'image' => 'required',
            'code'  => 'required'
        ]);

        $s = Setting::first();
        if($request->code == $s->access_code){

            $delegate = session('delegate');

            $image = $request->image;
            $arr1 = explode(";", $image);
            $arr2 = explode(",", $arr1[1]);
            $data = base64_decode($arr2[1]);


            $filename = 'profile/' . $delegate->firstname . '-' . $delegate->lastname . '-' . md5($delegate->email) . '.png';

            Storage::put($filename, $data);

            return response(['status' => true, 'message' => 'Upload successful'], 200);
        } else {
            return response(['status' => false, 'message' => 'Incorrect code'], 200);
        }
    }

    public function downloadBadge()
    {
        $delegate = Session::get('delegate');
        $fullname = $delegate->title . ' ' . $delegate->firstname . ' ' . $delegate->lastname;

        $profilePic = $delegate->firstname . '-' . $delegate->lastname . '-' . md5($delegate->email) . '.png';

        $badgeCode = $this->generateBadgeCode($delegate->id);
        $pdf = new Fpdf();

        $pdf->AddPage();
        $pdf->SetTextColor(255,255,255);

        $pdf->Image(url('assets/images/badge.jpg'), 25, 40, 150);

        $pdf->Image(storage_path('app/profile/' . $profilePic), 48.5, 50, 25);

        $pdf->SetXY(25, 70);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(75,20, $fullname, 0, '', 'C');

        $pdf->SetTextColor(255,171,0);
        $pdf->SetXY(25, 75);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(75,20, ucfirst($delegate['role']), 0, '', 'C');

        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(25, 105);
        $pdf->SetFont('Arial', '', 7);
        $pdf->Cell(75,20, 'KINGSCHAT ID', 0, '', 'C');

        $pdf->SetTextColor(20, 4, 80);
        $pdf->SetXY(25, 113);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(75,10, ucfirst($delegate['kcid']), 0, '', 'C');

        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(25, 115);
        $pdf->SetFont('Arial', '', 7);
        $pdf->Cell(75,20, 'BADGE NO', 0, '', 'C');

        $pdf->SetTextColor(20, 4, 80);
        $pdf->SetXY(25, 123);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(75,10, $badgeCode, 0, '', 'C');

        $pdf->SetTextColor(250, 0, 50);
        $pdf->SetXY(25, 125);
        $pdf->SetFont('Arial', '', 7);
        $pdf->Cell(75,20, 'ISSUE DATE', 0, '', 'C');

        $pdf->SetTextColor(20, 4, 80);
        $pdf->SetXY(25, 133);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(75,10, date('d/m/Y'), 0, '', 'C');

        $pdf->Output('I', $fullname . '.pdf');
    }

    private function generateBadgeCode($id){
        $d = Delegate::find($id);
        if($d->badge_code != null && (strlen($d->badge_code) > 0)){
            return $d->badge_code;
        } else {
            $code = '';
            for($i = 1; $i > 0; $i++){
                $code = 'LWFS' . date('Y') .'/' . rand(1000, 99999);
                $exists = Delegate::where('badge_code', $code)->exists();
                if(!$exists){
                    $d->badge_code = $code;
                    $d->save();
                    break;
                }
            }
            return $code;
        }
    }

    public function resources()
    {
        $role = session('delegate')->role;
        $data['resources'] = Resource::where('access_level', $role)->orWhere('access_level', 'all')->get();
        $data['page_title'] = 'Resources';
        $data['resource_menu'] = true;
        return view('resources', $data);
    }

    public function downloadResource($id)
    {
        $delegate = Session::get('delegate');
        $res = Resource::findOrFail($id);
        if(in_array($res->access_level, [$delegate->role, 'all'])){
            $downloaded = DownloadedRes::where('delegate_id', $delegate->id)->where('file_id', $res->id)->exists();
            if(!$downloaded){
                $downloaded = new DownloadedRes();
                $downloaded->delegate_id = $delegate->id;
                $downloaded->file_id = $res->id;
                $downloaded->save();
            }
            return response()->download(storage_path("app/public/{$res->filename}"));
        } else {
            abort(404);
            return false;
        }
    }

    public function assignments()
    {
        $isSubmitted = TempSubmit::where('delegate_id', Session::get('delegate')->id)->exists();
        if($isSubmitted){
            return redirect()->back()->with('message', 'Assignment has been submitted already');
        } else {
            $data['page_title'] = 'Assignment';
            $data['assignment_menu'] = true;
            return view('assignments', $data);
        }
    }

    public function uploadAssignment(Request $request)
    {
        $request->validate([
            'document'  => 'required|file'
        ]);

        $delegate = Session::get('delegate');
        $ext = $request->file('document')->getClientOriginalExtension();
        $file = $request->file('document')->store('assignment', 'public');

        $n = new TempSubmit();
        $n->delegate_id = $delegate->id;
        $n->filename = $file;
        $n->extension = $ext;
        $n->save();

        return redirect()->route('home')->with('message', 'Assignment submitted');
    }

}
