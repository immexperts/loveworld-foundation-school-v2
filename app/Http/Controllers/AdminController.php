<?php

namespace App\Http\Controllers;

use App\Models\Delegate;
use App\Models\Report;
use App\Models\ReportDate;
use App\Models\Resource;
use App\Models\Setting;
use App\Models\TempSubmit;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard()
    {
        $data['dashboard_menu'] = true;
        $data['teachers'] = Delegate::where('role', 'teacher')->count();
        $data['principals'] = Delegate::where('role', 'principal')->count();
        $data['page_title'] = 'Dashboard';
        return view('admin.dashboard', $data);
    }

    public function reports()
    {
        $months = [];
        $years = [];
        for($i = 1; $i <= 12; $i++){
            $dateObj   = \DateTime::createFromFormat('!m', $i);
            $monthName = $dateObj->format('F');
            array_push($months, [
                'val' => $i,
                'title' => $monthName
            ]);
        }

        if(date('Y') == 2021){
            array_push($years, 2021);
        } else {
            for($y = date('Y'); $y >= 2021; $y--){
                array_push($years, $y);
            }
        }

        $data['years'] = $years;
        $data['months'] = $months;
        $data['report_menu'] = true;
        $data['page_title'] = 'Weekly Reports';
        return view('admin.reports', $data);
    }

    public function fetchReports(Request $request)
    {
        $request->validate([
            'year'  => 'required',
            'month' => 'required',
            'week'  => 'required'
        ]);

        $reports = Report::where('month', $request->month)->where('week', $request->week)->where('year', $request->year)
            ->join('delegates', 'delegates.id', '=', 'reports.delegate_id')
            ->join('zones', 'delegates.zone_id', '=', 'zones.id')->get();

        return response(['status' => true, 'data' => [
            'report'    => $reports,
            'title'     => 'Report for ' . gmdate("F", strtotime("2021-$request->month-10")) . ' Week ' . $request->week . ' - ' . $request->year,
            'link'      => route('downloadReports', [$request->year, $request->month, $request->week])
        ]], 200);
    }

    public function downloadReports($year, $month, $week)
    {
        $fileName = 'Report for ' . gmdate("F", strtotime("2021-$month-10")) . ' Week ' . $week . ' - ' . $year . '.csv';
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $reports = Report::where('month', $month)->where('week', $week)->where('year', $year)
            ->join('delegates', 'delegates.id', '=', 'reports.delegate_id')
            ->join('zones', 'delegates.zone_id', '=', 'zones.id')->get();

        if(count($reports) > 0){
            $columns = array(
                'Principal', 'Zone', 'No in Foundation School', 'Zonal Capacity', 'Attendance',
                'Class 1', 'Class 2', 'Class 3', 'Class 4', 'Class 5', 'Class 6', 'Class 7', 'Eligible for Graduation'
            );

            $callback = function () use ($reports, $columns){
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach($reports as $report){
                    $row = [
                        $report->title . ' ' . $report->firstname . ' ' . $report->lastname,
                        $report->zone_name,
                        $report->fs_number,
                        $report->fs_capacity,
                        $report->week_att,
                        $report->class_1,
                        $report->class_2,
                        $report->class_3,
                        $report->class_4,
                        $report->class_5,
                        $report->class_6,
                        $report->class_7,
                        $report->eligible_grad,
                    ];

                    fputcsv($file, $row);
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        } else {
            return redirect()->back();
        }
    }

    public function newReportDate(Request $request)
    {
        $request->validate([
            'year'  => 'required',
            'month' => 'required',
            'week'  => 'required'
        ]);

        $exists = ReportDate::whereMonthAndWeekAndYear($request->month, $request->week, $request->year)->exists();
        if($exists){
            return redirect()->back()->with('error', 'Report date exists already');
        } else {
            $new = new ReportDate();
            $new->month = $request->month;
            $new->week = $request->week;
            $new->year = $request->year;
            $new->save();

            return redirect()->back()->with('message', 'Report date added');
        }
    }

    public function principals()
    {
        $data['delegate_menu'] = true;
        $data['page_title'] = 'Principals';
        $data['principals'] = Delegate::where('delegates.role', 'principal')
            ->join('zones', 'zones.id', '=', 'delegates.zone_id')->select('delegates.*', 'zones.zone_name')->get();
        return view('admin.principals', $data);
    }

    public function teachers()
    {
        $data['delegate_menu'] = true;
        $data['page_title'] = 'Teachers';
        $data['teachers'] = Delegate::where('delegates.role', 'teacher')
            ->join('zones', 'zones.id', '=', 'delegates.zone_id')->select('delegates.*', 'zones.zone_name')->get();
        return view('admin.teachers', $data);
    }

    public function assignments()
    {
        $assignments = TempSubmit::join('delegates', 'delegates.id', '=', 'temp_submissions.delegate_id')
            ->join('zones', 'zones.id', '=', 'delegates.zone_id')
            ->select('temp_submissions.*', 'delegates.title', 'delegates.firstname', 'delegates.lastname', 'zones.zone_name')->get();
        $data['assignment_menu'] = true;
        $data['page_title'] = 'Assignment Submissions';
        $data['assignments'] = $assignments;
        return view('admin.assignments', $data);
    }

    public function downloadAssignment($id)
    {
        $file = TempSubmit::findOrFail($id);
        return response()->download(storage_path("app/public/{$file->filename}"));
    }

    public function resources()
    {
        $data['resources'] = Resource::all();
        $data['resource_menu'] = true;
        $data['page_title'] = 'Resources';
        return view('admin.resources', $data);
    }

    public function uploadResource(Request $request)
    {
        $request->validate([
            'title'         => 'required',
            'file'          => 'required|file',
            'access_level'  => 'required'
        ]);

        $ext = $request->file('file')->getClientOriginalExtension();
        $file = $request->file('file')->store('resources', 'public');

        $res = new Resource();
        $res->title = $request->title;
        $res->extension = $ext;
        $res->filename = $file;
        $res->access_level = $request->access_level;
        $res->save();

        return redirect()->back()->with('message', 'Resource uploaded successfully');

    }

    public function securityPage()
    {
        $data['page_title'] = 'Security Settings';
        $data['setting_menu'] = true;
        $data['setting'] = Setting::first();
        return view('admin.security', $data);
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'old_password'  => 'required',
            'password'      => 'required|confirmed'
        ]);

        $s = Setting::first();
        $validated = password_verify($request->old_password, $s->password);
        if($validated){
            $s->password = password_hash($request->password, PASSWORD_DEFAULT);
            $s->save();
            session()->put('admin', $s);
            return redirect()->back()->with('message', 'Password changed successfully');
        } else {
            return redirect()->back()->with('error', 'Incorrect password');
        }
    }

    public function changeAccessCode(Request $request)
    {
        $request->validate([
            'access_code' => 'required'
        ]);

        $s = Setting::first();
        $s->access_code = $request->access_code;
        $s->save();
        return redirect()->back()->with('message', 'Access code saved');
    }
}
